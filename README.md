Multiple Choice Quiz
Challenge Description

Create an application that simulates a simple quiz with multiple-choice questions.
Requirements

    Display a question with four answer options.
    Allow users to select one answer option.
    Provide a "Submit" button to submit the user's answer.
    Display a message indicating whether the user's answer is correct or incorrect.
    Provide a "Next" button to load the next question.
    Keep track of the user's score throughout the quiz.
    Display the final score after the last question.

