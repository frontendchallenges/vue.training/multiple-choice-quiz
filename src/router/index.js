import { createRouter, createWebHistory } from "vue-router";
import ResultsComponent from "../ResultsComponent.vue";
import Quiz from "../Quiz.vue";

const routes = [
    { path: '/', component: Quiz },
    { path: '/results/:g', component: ResultsComponent, name: 'results', props: true }
];

const router = createRouter({ history: createWebHistory(), routes });
export default router;